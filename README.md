# jQuery swipe event #

The jQuery swipe event plugin adds a swipeLeft, swipeRight, swipeUp, and swipeDown event to jQuery.

### To use this plugin: ###

* First add jQuery to your project.
* Second add the jQuery swipe event plugin to your project beneath jQuery.

### Examples of use ###

$('div').swipeRight(function(){
   -- your logic here --
});

$('div').swipeLeft(function(){
   -- your logic here --
});

$('div').swipeUp(function(){
   -- your logic here --
});

$('div').swipeDown(function(){
   -- your logic here --
});

### dependencies ###

* jQuery